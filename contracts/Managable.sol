pragma solidity ^0.4.21;

contract Managable {
	
	/// VARIABLES ///

	/**
	 * Address of admin
	*/
	address public admin;

	/**
	 * Address of server 
	*/
	address public server;

	/// EVENTS ///

	event AdminChanged(address _to);
	event ServerChanged(address _to);

	/// MODIFIERS ///

	modifier onlyAdmin() {
		require(msg.sender == admin);
		_;
	}

	modifier onlyServer() {
		require(msg.sender == server);
		_;
	}

	/// FUNCTIONS ///

	constructor() {
		admin = msg.sender;
	}

	function changeAdminAddress(address _to) public onlyAdmin {
		require(_to != 0x0);
		admin = _to;
		emit AdminChanged(_to);
	}

	function changeServerAddress(address _to) public onlyAdmin {
		require(_to != 0x0);
		server = _to;
		emit ServerChanged(_to);
	}

}