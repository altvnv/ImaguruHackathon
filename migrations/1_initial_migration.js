var Migrations = artifacts.require("./Migrations.sol");
var DMToken = artifacts.require("./DMToken.sol");
var DMLToken = artifacts.require("./DMTokenLocalized.sol");

module.exports = function(deployer) {
  deployer.deploy(Migrations);
  deployer.deploy(DMToken);
  deployer.deploy(DMLToken);
};
