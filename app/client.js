const web3 = require('web3');
const contract = require('truffle-contract');
const arts = require('../build/contracts/DMTokenLocalized.json');

let DMToken;

async function init() {
	DMToken.setProvider(new web3.providers.HttpProvider("http://localhost:9545"));
}

async function addLoanInfo(link, amount, companies) {
	let index = await DMToken.addLoan(link, amount, companies, 0);
	return index;
}

async function acceptLoan(index) {
	await DMToken.acceptLoan(index);
}

async function burnLoan(index) {
	await DMToken.burnLoan();
}